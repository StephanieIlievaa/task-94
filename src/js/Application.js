import EventEmitter from "eventemitter3";

export default class Application extends EventEmitter {
  static get events() {
    return {
      READY: "ready",
    };
  }

  constructor() {
    super();
    this.emojis = [];
    this.banana = "🍌";
    this.emit(Application.events.READY); 
    this.div = document.getElementById('emojis');
  }

  setEmojis(emojis) {
    this.emojis = emojis;
    this.removeAllChildNodes(this.div);

    //create a paragraph, and set it`s textContent
    this.emojis.forEach(emoji => {
      let paragraph = document.createElement('p');
      paragraph.textContent = emoji;

      this.div.appendChild(paragraph);
    });
  }
  //implement map logic
  addBananas() {
    let monkeys = this.emojis.map(monkey => {
      return monkey += this.banana;
    });

    this.setEmojis(monkeys);

  }
  //remove all children of the node
  removeAllChildNodes(parent) {
    while (parent.firstChild) {
      parent.removeChild(parent.firstChild);

    }
  }
}