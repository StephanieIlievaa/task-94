# ⚙ HTML, CSS and SASS Boilerplate 
This project is used as a boilerplate for tasks in the "HTML, CSS and SASS" course in boom.dev

🤯💥💣

## JavaScript Task
* In this task, we have 3 hungry monkeys and we'll have to give each of them a banana. You'll receive an **emojis** array which contains the 3 monkeys. With the help of the array **map** method we will create a new array where all the monkeys will receive a banana.
## Objective
* Checkout the dev branch.
* Add a banana to each array element(monkey) and then display each element of the modified array in a browser.
* Before displaying the modified array(mokeys + banana) we have to clear the content of the wrapper **div** with an id of **emojis**.
* To display the elements in a browser, create a **paragraph** element, set its **textContent** to be equal to the modified array element, and then append it to the wrapper div.

## Requirements
* Start the project with **npm run start**.
* In the **addBananas()** method we have to implement the **map** logic.
* In the **addEmojis()** method we have to implement the logic for displaying the emojis in a browser.
* When implemented merge the dev branch to master.

